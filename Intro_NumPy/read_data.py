import numpy as np

csv_filename = "boston_houses.csv"


def main():
    data = np.loadtxt(csv_filename, delimiter=',', skiprows=1, dtype='float64')  # чтение данных

    print('Размерность (SHAPE): {}  Тип: {}'.format(data.shape, data.dtype))  # Размерность

    print('Вектор средних арифм. значений (по столбцам): ', data.mean(axis=0))
    print('Вектор мин. значений(по столбцам): ', np.min(data, axis=0))
    print('Вектор макс. значений(по столбцам): ', np.max(data, axis=0))
    print('Вектор суммы (по столбцам):', data.sum(axis=0))

    print('Вектор суммы (по строкам): {} ... и т.д.'.format(data.sum(axis=1)[:5]))
    # print('Exp: {} \n ... .. .'.format(np.exp(data)[:2]))
    print('Стандарт. отклонение (по столбцам): ', data.std(axis=0))
    print('Гиперболический синус средних арифм. значений: ', np.sinh(data.mean(axis=0)))

    # print('Вектор вариации(дисперсия элементов массива) (по столбцам): ', array.var(axis=0))
    # print('округление : ', np.rint(array))
    # print('Sin: {} \n .....'.format(np.sin(array)[:2]))


if __name__ == '__main__':
    main()


# ---------------------------------------------------
    # print('Вектор средних арифм. значений (по столбцам): ', np.mean(data, axis=0))
    # print('Вектор мин. значений(по столбцам): ', data.min(axis=0))
    # print('Вектор макс. значений(по столбцам): ', data.max(axis=0))
# ---------------------------------------------------
