import random
import string

MAX_LENGTH_PASSW = 20  # не меньше 6 !!!


def get_cyr_chars():
    """ Геннерация киррилицы по номеру символа в таблице Unicode. """
    lowercase = [chr(i) for i in range(1072, 1104)]  # получаем список строчных букв
    lowercase.append(chr(1105))  # добавляем "ё"
    lowercase = ''.join(lowercase)  # преобразуем в строку
    uppercase = [chr(i) for i in range(1040, 1072)]  # получаем список прописных букв
    uppercase.append(chr(1025))  # добавляем "Ё"
    uppercase = ''.join(uppercase)
    return lowercase, uppercase


def gen_paswd(passw_comp, length_passw):  # генерация одного пароля
    if length_passw == 0:
        length_passw = random.randint(len(passw_comp), MAX_LENGTH_PASSW)

    length_passw_set = 0  # длина пароля в наборе
    passw = []  # Формируемый пароль
    possible_length = length_passw - len(passw_comp)  # возможная длина пароля + 1 для текущего набора в переборе.
    for i, item in enumerate(passw_comp):  # перебираем наборы
        possible_length = possible_length - length_passw_set + 1  # подсчет возможной длины для набора
        if i == len(passw_comp) - 1:  # проверка на последний набор
            length_passw_set = possible_length  # определяем остаток длины от пароля для последнего набора
        else:  # иначе выбирараем случайное в возможном диапазоне
            length_passw_set = random.randint(1, possible_length)  # определяем случайную длину
        passw.extend(random.choices(item, k=length_passw_set))  # формируем выборку с длиной ..., и расширяем пароль
    random.shuffle(passw)  # перемешиваем полученный пароль
    return ''.join(passw)  # преобразуем в строку и возвращаем


lat_chars_low = string.ascii_lowercase  # латинские строчные буквы
lat_chars_up = string.ascii_uppercase  # латинские прописные буквы
digit_chars = string.digits  # цифры
cyr_chars_low, cyr_chars_up = get_cyr_chars()  # кириллица
punct_chars = string.punctuation  # спец символы (знаки пуктуации)

# Cостав пароля (password composition)
passw_comp = [lat_chars_low, lat_chars_up, digit_chars, cyr_chars_low, cyr_chars_up, punct_chars]
# print(passw_comp)
""" Будем в каждом наборе генерировать кол-во случайных символов. 
Кол-во этих символов будет генерироваться тоже случайно, 
но так чтобы общее кол-во каждого набора не превышала введенную длину пароля. 
Полученные символы для каждого набора в конце случайно перемешиваются"""

print(f' *** Программа генерация паролей. ***')
print(f'* Кол-во используемых наборов: {len(passw_comp)} (латинские стр. проп., '
      f'цифры, кириллица стр. проп, спец. символы) *')
print(f'* Если длина пароля не задана или не верна, то генерация длины будет '
      f'случайным от {len(passw_comp)} до {MAX_LENGTH_PASSW} знаков *\n')
while True:
    try:
        numb_passw = int(input('Введите количество паролей: '))
        break
    except ValueError:
        print('Введите  число!!!')

length_passw = input(f'Введите длину пароля (от 6 до {MAX_LENGTH_PASSW} символов): ')
try:
    length_passw = int(length_passw)
except ValueError:
    length_passw = 0

if len(passw_comp) > length_passw or length_passw > MAX_LENGTH_PASSW:
    length_passw = 0

print('\n\tСписок сформированных паролей:')
for _ in range(1, numb_passw + 1):
    result = gen_paswd(passw_comp, length_passw)
    print(f"Пароль № {_:2}:\t {result} \t\t- Длина: {len(result)}")



# --------------
# print('------------------------------\n', i, ':  ', item)
# print(f'Возможная длина пароля: {possible_length} , кол-во знаком в этом наборе: {a}')
# print('сумма использованной длины: ', suma)
# print(random.choices(item, k=a))
    # print(f"Пароль № {_}: {result} {str_len:>30}")
# for _ in range(a):
#     passw.append(random.choice(item))
# ----------------
    # str_len = f'Длина: {len(result)}'
# -----------------------------
# while True:
#     numb_passw = input('Введите количество паролей: ')
#     length_passw = input('Введите длину пароля: ')
#
#     if not numb_passw.isdigit() or not length_passw.isdigit():
#         print('Введите  число!!!')
#         # exit()
#     else:
#         numb_passw = int(numb_passw)
#         length_passw = int(length_passw)
#         break
# -----------------------------
# lat_chars = string.ascii_letters  # латинские прописные и строчные буквы
# middle = int((len(lat_chars))/2)
# lat_chars_low = lat_chars[0:middle]  # латинские строчные буквы
# -----------------------------
# number = int(number)
# length = int(length)
#
#
# for n in range(number):
#     password =''
#     for i in range(length):
#         password += random.choice(chars)
#     print(password)

# passw = [random.choice(item) for _ in range(1, a)]
# passw = random.choice(item)
# ---------------------------------------------------------------
# print(len(lat_chars))
# print(len(digit_chars))
# print(len(cyr_chars))
# print(len(punct_chars))
# -----------------------------------
# -----------------------------------
# -----------------------------------
# chars = '+-/*!&$#?=@<>abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
# l, w, h = [float(i) for i in body_whl.split('x')]
# print(lowercase, len(lowercase))
# print(uppercase, len(uppercase))
# print(get_cyr_chars.__doc__)
